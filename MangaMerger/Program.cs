﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Drawing;
using System.Diagnostics;

namespace MangaMerger
{
    class Program
    {
        const string fileFilter = "*.jpg|*.jpeg|*.png";
        static readonly char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };


        static void Main(string[] args)
        {
            string source = args[0];
            string target = args[1];
            if (Directory.Exists(source))
            {
                //Ordner mit Unterordner = Stapelverarbeitung
                if (Directory.GetDirectories(source).Length > 0)
                {
                    foreach (string dir in Directory.GetDirectories(source))
                    {
                        processDir(dir, target);
                    }
                }
                else
                {
                    processDir(source, target);
                }
            }
            Console.WriteLine("Finished Quee");
            Console.Read();
        }

        static void processDir(string dir, string tar)
        {
            DirectoryInfo dirinf = new DirectoryInfo(dir);
            long index = 1;

            Console.WriteLine("Process " + dir);

            var pages = GetFiles(dirinf, fileFilter);

            if (pages.Length == 0)
            {
                Console.WriteLine("No Pages found");
                return;
            }
            Console.WriteLine(pages.Length + " Pages found");

            DirectoryInfo inf = new DirectoryInfo(dir);
            string name = inf.Name + ".pdf";

            string path = Path.Combine(tar, name);
            PdfDocument doc = new PdfDocument();

            foreach (var page in pages)
            {
                Console.WriteLine("Create Page " + index);
                XImage img = XImage.FromFile(page.FullName);

                PdfPage nPage = doc.AddPage();
                nPage.Width = img.Size.Width;
                nPage.Height = img.Size.Height;
                XGraphics gfx = XGraphics.FromPdfPage(nPage);
                gfx.DrawImage(img, new XRect(img.Size));
                nPage.Close();
                index++;
            }

            doc.Save(path);
            Console.WriteLine("Finished creating Document");
        }

        static FileInfo[] GetFiles(DirectoryInfo dirinf, string searchPattern)
        {
            List<FileInfo> files = new List<FileInfo>();
            foreach (string pattern in searchPattern.Split('|'))
                files.AddRange(dirinf.GetFiles(pattern));

            return files.Distinct(new CompareFiles()).OrderBy(f => extractId(f.Name)).ToArray();
        }

        static int extractId(string name)
        {
            string dig = "";
            if (name.Any(char.IsDigit))
            {
                int start = name.IndexOfAny(digits);
                for (int i = start; i < name.Length && char.IsDigit(name[i]); i++)
                    dig += name[i];

                return int.Parse(dig);
            }
            return 0;
        }
    }

    class CompareFiles : IEqualityComparer<FileInfo>
    {
        bool IEqualityComparer<FileInfo>.Equals(FileInfo x, FileInfo y)
        {
            return Path.GetFullPath(x.FullName) == Path.GetFullPath(y.FullName);
        }

        int IEqualityComparer<FileInfo>.GetHashCode(FileInfo obj)
        {
            return obj.FullName.GetHashCode();
        }
    }
}